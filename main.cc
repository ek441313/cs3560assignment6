// Brock Ferrell
// CS2401
// November 23, 2015
// Project7

/**
@file Main.cc
@brief File containing main which starts the game
@author Eric Kelly
*/

#include "game.h"
#include "othello.h"
using namespace main_savitch_14;


int main()
{
	Othello theGame;
	/**
	@brief Restart function that restarts the game board
	*/
	theGame.restart();
	theGame.play();
}
