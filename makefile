all:	
	make build
	make doc

build:
	make game

game: othello.o game.o main.o
	g++ othello.o game.o main.o -o game
othello.o: othello.h othello.cc game.h piece.h colors.h
	g++ -c othello.cc 
game.o: game.h game.cc
	g++ -c game.cc
main.o: main.cc
	g++ -c main.cc

doc:
	make html
	make latex
html: piece.h othello.h othello.cc main.cc game.h game.cc colors.h
	doxygen Doxyfile
latex: piece.h othello.h othello.cc main.cc game.h game.cc colors.h
	doxygen Doxyfile

clean:
	rm -f *.o othello.o game.o main.o
	rm -rf html latex
