// Brock Ferrell
// CS2401
// November 23, 2015
// Project7
/**
@file Piece.h
@brief File containing the class for a single piece of the game board
@author Eric Kelly
*/
#ifndef PIECE_H
#define PIECE_H
enum color {black, white, blank};

/**
@brief Piece class that changes the color of a piece between black and white using the flip function, uses colors.h
*/
class piece {
public:
	piece() {theColor = blank;}
	
	void flip()
	{
		if (theColor == white) {
			theColor = black;
		}
		else if (theColor == black) {
			theColor = white;
		}
	}

	bool is_blank()const {return theColor == blank;}
	bool is_black()const {return theColor == black;}
	bool is_white()const {return theColor == white;}
	void set_white() {theColor = white;}
	void set_black() {theColor = black;}

private:
	color theColor;

};

#endif

